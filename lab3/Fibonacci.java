public class Fibonacci {
	public static void main(String[] args){
			int limit = Integer.parseInt(args[0]);
		int element = 0;
		int next = 1;
		System.out.print(element + " " + next);		
		while (element + next < limit){
			int total = element + next;
			System.out.print(" " + total);	
			element = next;			
			next = total;	
		}

		System.out.println();
	}

}
