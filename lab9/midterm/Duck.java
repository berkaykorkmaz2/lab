package midterm;

public class Duck extends Animal {

	public Duck(String name) {
		super(name);
	}

	@Override
	public String speak() {
		return "Quack";
	}

}
