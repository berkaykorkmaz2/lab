package midterm;

public class Question2 {

	public static int max(int a, int b){
		if (a>b)
			return a;
		return b;
	}
	
	
	public static void main(String[] args) {
		System.out.println(max(10,12));
		System.out.println(max(17,12));
		System.out.println(max(0,3));
		System.out.println(max(-7,-9));
		System.out.println(max(-7,9));
	}

}
