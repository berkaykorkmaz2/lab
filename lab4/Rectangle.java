
public class Rectangle {
	int sideA,sideB;
	public Rectangle(int a,int b){
		sideA = a;
		sideB = b;
	}
	public int area(){
		return sideA*sideB;
	}
	public int perimeter(){
		return (sideA+sideB)*2;
	}
}
