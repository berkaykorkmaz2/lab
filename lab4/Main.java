
public class Main {

	public static void main(String[] args) {
		
		Rectangle rect = new Rectangle(5,6);
		
		int rectArea = rect.area();
		int rectPerimeter = rect.perimeter(); 
		
		System.out.println("Rectangle");
		System.out.println("Area: " + rectArea + " " + "Perimeter: " + rectPerimeter);
	
		Circle circle = new Circle(10);
		
		double circleArea = circle.area();
		double circlePerimeter = circle.perimeter();
		
		System.out.println("Circle");
		System.out.println("Area: " + circleArea + " " + "Perimeter: " + circlePerimeter);
		
		Circle circle2 = new Circle(10);
		System.out.println(circle2.area());
	
	}

}
