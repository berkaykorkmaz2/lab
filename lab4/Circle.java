
public class Circle {
	int radius;
	double pi = 3.14;
	public Circle(int r){
		radius = r;
	}
	public double area(){
		return pi * radius * radius;
	}
	public double perimeter(){
		return 2 * pi * radius;
	}
}
