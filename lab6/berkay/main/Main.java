package berkay.main;

import berkay.shapes.*;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		Circle circle = new Circle(5);
		System.out.println(circle.area());
		System.out.println(circle.getRadius());
		
		Rectangle rectangle = new Rectangle(5,6);
		System.out.println(rectangle.area());
		System.out.println();
		
		
		ArrayList<Circle> circles = new ArrayList<Circle>();
		circles.add(circle);
		circles.add(new Circle(6));
		circles.add(new Circle(7));
		
		Drawing drawing = new Drawing();
		for(int i = 0; i < circles.size(); i++){
			Circle circ = circles.get(i);
			//System.out.println(circ.area());
			drawing.addCircle(circ);
		}
		
		drawing.printAreas();
		System.out.println();
		drawing.printRadiuses();
	}

}
