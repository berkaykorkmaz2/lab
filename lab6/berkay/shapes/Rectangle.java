package berkay.shapes;

public class Rectangle {
	public int side1;
	public int side2;
	public Rectangle(int side1 , int side2) {
		this.side1 = side1;
		this.side2 = side2;
	}
	public double area(){
		return side1 * side2;
	}
	
}
