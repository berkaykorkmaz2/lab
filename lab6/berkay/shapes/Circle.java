package berkay.shapes;

public class Circle {
	
	private int radius;
	
	public Circle(int r){
		radius = r;
	}
	
	public double area(){
		return radius * radius * Math.PI;
	}
	
	public int getRadius(){
		return radius;
	}
	
}
