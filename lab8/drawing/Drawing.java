package drawing;

import java.util.ArrayList;

public class Drawing {
	
	private ArrayList<Shape> shapes = new ArrayList<Shape>();

	
	public double totalArea(){
		double totalArea = 0;
		
		for(Shape shape: shapes){
			if (shape instanceof Circle){
				Circle circ = (Circle) shape;
				totalArea += circ.area();
			}else if(shape instanceof Rectangle){
				Rectangle rect = (Rectangle) shape;
				totalArea += rect.area();
			}else if (shape instanceof Square){
				Square sq = (Square) shape;
				totalArea += sq.area();
			}

		}
		
		return totalArea;
	}
	
	
	public void draw(){
		
		for(Shape shape: shapes){
			if (shape instanceof Circle){
				Circle circ = (Circle) shape;
				circ.draw();
			}else if(shape instanceof Rectangle){
				Rectangle rect = (Rectangle) shape;
				rect.draw();
			}else if (shape instanceof Square){
				Square sq = (Square) shape;
				sq.draw();
			}

		}
	}

	
	public void move(int xDistance,  int yDistance){
		
		
		
		for(Shape shape: shapes){
			if (shape instanceof Circle){
				Circle circ = (Circle) shape;
				circ.move(xDistance, yDistance);
			}else if(shape instanceof Rectangle){
				Rectangle rect = (Rectangle) shape;
				rect.move(xDistance, yDistance);
			}else if (shape instanceof Square){
				Square sq = (Square) shape;
				sq.move(xDistance, yDistance);
			}

		}
	}
	
	public void addShape(Shape shape){
		shapes.add(shape);
	}
	
	
}