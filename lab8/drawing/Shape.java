package drawing;

public abstract class Shape {
	abstract void draw();
	abstract void move(int xDistance,  int yDistance);
	abstract double area();
}
