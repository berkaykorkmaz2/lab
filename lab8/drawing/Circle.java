package drawing;

public class Circle extends Shape {
	
	private int radius;
	private Point center;
	
	public Circle(int radius, Point  center){
		this.radius = radius;
		this.center = center;
	}
	@Override
	public void draw(){
		System.out.println("Drawing Circle at " + center + " having radius " + radius);
	}
	@Override
	public double area(){
		return Math.PI * radius * radius;
	}
	@Override
	public void move(int xDistance,  int yDistance){
		center.move(xDistance, yDistance);
	}
}